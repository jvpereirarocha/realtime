var nome_sala = {{ nome_sala_json }}

var chatSocket = new WebSocket(
    'ws://' + window.location.host + '/ws/chat/' + nome_sala + '/'
);

chatSocket.onmessage = function(e) {
    var dados = JSON.parse(e.data);
    var mensagem = dados['mensagem'];
    document.querySelector('#sala').value += (mensagem + '\n');
};

chatSocket.onclose = function(e) {
  console.error('O chat encerrou');
}

document.querySelector('#texto').focus();
document.querySelector('#texto').onkeyup = function(e) {
    document.querySelector('#botao').click();
}

document.querySelector('#botao').onclick = function(e) {
    var mensagemInput = document.querySelector('#texto');
    var mensagem = mensagemInput.value;
    chatSocket.send(JSON.stringify({
        'mensagem': mensagem
    }));
    mensagemInput.value = '';
}
