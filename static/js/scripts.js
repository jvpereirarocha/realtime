document.querySelector('#nome_sala').focus();

document.querySelector('#nome_sala').onkeyup = function(e) {
    if (e.keyCode === 13) {
        document.querySelector('#botao').click();
    }
};

document.querySelector('#botao').onclick = function(e) {
  var nome_sala = document.querySelector('#nome_sala').value;
  if (nome_sala != "") {
      window.location.pathname = '/chat/' + nome_sala + '/';
  }
  else {
      alert('Você precisa informar o nome da sala');
      document.querySelector('#nome_sala').focus();
  }
};
